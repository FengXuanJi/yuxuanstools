<?php

namespace Muyuxuan\wechat;

use Muyuxuan\wechat\account\OfficialAccount;
use Muyuxuan\wechat\mini\MiniProgram;
use Muyuxuan\wechat\mini\Service;

class WeChat
{
    protected $domain = 'https://api.weixin.qq.com';
    use Other;
    //微信小程序
    public static function miniProgram(array $config=[]){
        return new MiniProgram($config);
    }
    //微信公众号
    public static function officialAccount(array $config=[]){
        return new OfficialAccount($config);
    }
}