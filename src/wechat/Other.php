<?php

namespace Muyuxuan\wechat;

trait Other
{
    protected $config = null;
    protected $session_key = null;
    protected $message = '';//用户消息
    protected $result = null;//结果
    protected $access_token = null;

    /**
     *
     * @param null $config
     */
    public function __construct(array $config=[])
    {
        if(!empty($config)){
            $this->config = $config;
        }
    }
    public function getMesssage(){
        return $this->message;
    }
    //获取结果
    public function getResult(){
        if(!empty($this->config['response_type'])){
            $this->config['response_type'] = 'array';
        }
        if(!empty($this->config['response_type'])){
            if($this->config['response_type']==='array'){
                if(is_string($this->result)){
                    $newresult = json_decode($this->result,true);
                    if($newresult!=null){
                        return $newresult;
                    }
                    $newresult = (array)json_decode($this->result);
                    if($newresult!=null){
                        return $newresult;
                    }
                }
            }elseif ($this->config['response_type']==='string'){
                if(is_array($this->result)){
                    $this->result = json_encode($this->result,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
                }
            }
        }
        return $this->result;
    }

    /**
     * 验证配置
     * @param array $data 必须的配置数组
     * @return bool
     */
    public function verify(array $data){
        $bool = true;
        foreach ($data as $k){
            if(empty($this->config[$k])){
                $this->message = $k."必填";
                $bool = false;
                break;
            }
        }
        return $bool;
    }
}