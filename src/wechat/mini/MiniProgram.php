<?php

namespace Muyuxuan\wechat\mini;

use GuzzleHttp\Client;
use Muyuxuan\http\Http;
use Muyuxuan\wechat\WeChat;

class MiniProgram extends WeChat
{
    /**
     * 小程序登录
     * @param string $code 用户code
     * @return array|false|mixed|string|null
     */
    public function login(string $code){
        $res = $this->verify([
            'app_id',
            'secret',
        ]);
        if($res===false){
            return false;
        }
        $url = $this->domain."/sns/jscode2session";
        $param = [
            'appid'=>$this->config['app_id'],
            'secret'=>$this->config['secret'],
            'js_code'=>$code,
            'grant_type'=>'authorization_code',
        ];
        $this->result = Http::get($url,$param);
        return $this->getResult();
    }

    /**
     * 获取acces_token
     * @return false|mixed|null
     */
    public function getAccessToken(){
        if(!empty($this->access_token)){
            return $this->access_token;
        }
        $res = $this->verify([
            'app_id',
            'secret',
        ]);
        if($res===false){
            return false;
        }
        $url = $this->domain."/cgi-bin/token";
        $param = [
            'grant_type'=>'client_credential',
            'appid'=>$this->config['app_id'],
            'secret'=>$this->config['secret'],
        ];
        $result = Http::get($url,$param);
        $result = json_decode($result,true);
        if(empty($result['access_token'])){
            $this->message = '获取access_token失败';
            return false;
        }
        $this->access_token = $result['access_token'];
        return $this->access_token;
    }

    /**
     * 获取scheme码
     * @param array $param 请求数据
     * @return array|false|mixed|string|null
     */
    public function getScheme(array $param=[]){
        $access_token = $this->getAccessToken();
        if($access_token===false){
            return false;
        }
        $url = $this->domain."/wxa/generatescheme?access_token=".$access_token;
        $this->result = Http::sendJson($url,$param);
        return $this->getResult();
    }

    /**
     * 获取小程序码
     * @param array $param 参数
     * @return false|void
     */
    public function getQRCode(array $param=[]){
        $access_token = $this->getAccessToken();
        if($access_token===false){
            return false;
        }
        $url = $this->domain."/wxa/getwxacode?access_token=".$access_token;
        $this->result = Http::sendJson($url,$param);
        return $this->getResult();
    }

    /**
     * 获取URLLink
     * @param array $param 参数
     * @return array|false|mixed|string|null
     */
    public function getUrlLink(array $param=[]){
        $access_token = $this->getAccessToken();
        if($access_token===false){
            return false;
        }
        $url = $this->domain."/wxa/generate_urllink?access_token=".$access_token;
        $this->result = Http::sendJson($url,$param);
        return $this->getResult();
    }

    /**
     * 查询 URL Link
     * @param array $param
     * @return array|false|mixed|string|null
     */
    public function queryUrlLink(array $param=[] ){
        $access_token = $this->getAccessToken();
        if($access_token===false){
            return false;
        }
       $url = $this->domain."/wxa/query_urllink?access_token=".$access_token;
       $this->result = Http::sendJson($url,$param);
        return $this->getResult();
    }

    /**
     * 获取用户手机号码
     * @param string $code
     * @return array|false|mixed|string|null
     */
    public function getPhoneNumber(string $code){
        $access_token = $this->getAccessToken();
        if($access_token===false){
            return false;
        }
        $param = [
            'code'=>$code
        ];
        $url = $this->domain."/wxa/business/getuserphonenumber?access_token=".$access_token;
        $this->result = Http::sendJson($url,$param);
        return $this->getResult();
    }

}