<?php

namespace Muyuxuan\time;

class MuTime
{
    /**
     * 获取月份的开始和结束时间
     * @param string|int|null $date 时间
     * @param bool $int 是否int类型
     * @return array
     */
    public static function getMonthTime(string|int $date=null,bool $int=false){
        if($date==null){
            $date = date("Y-m-d",time());
        }
        if(is_numeric($date)){
            $date = date("Y-m",$date);
        }
        $start = date("Y-m-01 00:00:00",strtotime($date));
        $end = date("Y-m-d 23:59:59",strtotime($start." +1 month -1 day"));
        if($int){
            return [strtotime($start),strtotime($end)];
        }
        return [$start,$end];
    }

    /**
     * 获取年份开始和结束
     * @param string|int|null $date 时间
     * @param bool $int 是否int类型
     * @return array
     */
    public static function getYearTime(string|int $date=null,bool $int=false){
        if($date==null){
            $date = date("Y-01-01",time());
        }else{
            if(is_numeric($date)){
                $date = date("Y-01-01",$date);
            }
            if(strlen($date)==4){
                $date = $date."-01-01";
            }elseif (strlen($date)==7){
                $date = $date."-01";
            }
        }
        $start = date("Y-01-01 00:00:00",strtotime($date));
        $end = date("Y-m-d 23:59:59",strtotime($start." +1 year -1 day"));
        if($int){
            return [strtotime($start),strtotime($end)];
        }
        return [$start,$end];
    }
    /**
     * 获取周开始和结束
     * @param string|int|null $date 时间
     * @param bool $int 是否int类型
     * @return array
     */
    public static function getWeekTime(string|int $date=null,bool $int=false){
        if($date==null){
            $date = date("Y-m-d");
        }
        if(is_numeric($date)){
            $date = date("Y-m-d",$date);
        }
        $date = date("Y-m-d",strtotime($date));
        $first=1;
        $w=date('w',strtotime($date));
        $start=date('Y-m-d 00:00:00',strtotime("$date -".($w ? $w - $first : 6).' days'));
        $end=date('Y-m-d 23:59:59',strtotime("$start +6 days"));
        if($int){
            return [strtotime($start),strtotime($end)];
        }
        return [$start,$end];
    }
    /**
     * 获取m摩天的前一天
     * @param string|int|null $date 时间
     * @param bool $int 是否int类型
     * @return array
     */
    public static function getYesterDay(string|int $date=null,bool $int=false){
        if($date==null){
            $date = date("Y-m-d");
        }
        if(is_numeric($date)){
            $date = date("Y-m-d",$date);
        }
        $start = date("Y-m-d 00:00:00",strtotime($date." -1 day"));
        $end = date("Y-m-d 23:59:59",strtotime($date." -1 day"));
        if($int){
            return [strtotime($start),strtotime($end)];
        }
        return [$start,$end];
    }
    /**
     * 获取某天的时间
     * @param string|int|null $date 时间
     * @param bool $int 是否int类型
     * @return array
     */
    public static function getDay(string|int $date=null,bool $int=false){
        if($date==null){
            $date = date("Y-m-d");
        }
        if(is_numeric($date)){
            $date = date("Y-m-d",$date);
        }
        $start = date("Y-m-d 00:00:00",strtotime($date));
        $end = date("Y-m-d 23:59:59",strtotime($date));
        if($int){
            return [strtotime($start),strtotime($end)];
        }
        return [$start,$end];
    }
}