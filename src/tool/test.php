<?php

use Muyuxuanpay\tool\Mtool;

require_once '../../vendor/autoload.php';
$a = [
    'long' => '116.43620200729366',
    'lat' => '39.916880160714435'
];
$b = [
    'long' => '116.4356870231628',
    'lat' => '39.908560377800676'
];
$res = Mtool::getDistance($a,$b,'m');




$str = 'PHP 字符串部分自定义替换星号（身份证、手机、姓名、IP等）实例';
echo Mtool::hidenStr($str,3).'<br/>'; // 第3个后的字符全部用*号代替
echo Mtool::hidenStr($str,3,'****').'<br/>'; // 第3个字符后指定用4个****
echo Mtool::hidenStr($str,3,-2,'*').'<br/>'; // 开始3至倒数第2位 中间全部替换*号
echo Mtool::hidenStr($str,3,-2,'****').'<br/>'; // 开始3至倒数第2位 指定填充 4个****

echo '<h3>2.身份证</h3>';
$idcard = '45072220081008821X';
echo Mtool::hidenStr($idcard,2).'<br/>'; // 第2个字符后隐藏
echo Mtool::hidenStr($idcard,2,'****').'<br/>'; // 第2个字符后 替换为4个****
echo Mtool::hidenStr($idcard,-4,'****').'<br/>'; // 指定后4位4个****
echo Mtool::hidenStr($idcard,5,8).'<br/>'; // 隐藏身份证中间8位

echo '<h3>3.手机号</h3>';
$mobile = 13800138000;
echo Mtool::hidenStr($mobile,2).'<br/>'; // 第2个字符后隐藏
echo Mtool::hidenStr($mobile,2,'****').'<br/>'; // 第2个字符后方指定用4个****代替
echo Mtool::hidenStr($mobile,2,-3).'<br/>'; // 手机前2位至后3位中间隐藏
echo Mtool::hidenStr($mobile,2,-3,'****').'<br/>'; // 手机前2位至后3位用4个星号代替

echo '<h3>4.银行卡</h3>';
//$bankCard = '6228888888888888123';
$bankCard = '6228123456789018123';
echo Mtool::hidenStr($bankCard,4).'<br/>'; // 第4个字符串后隐藏
echo Mtool::hidenStr($bankCard,4,'****').'<br/>'; // 第4个字符后 指定替换4个****

$str_hide = Mtool::hidenStr($bankCard,4,-3,'*'); // 前4位至后3位 中间全部替换*号
echo $str_hide.'<br/>';

$split_card = implode(' ',str_split($str_hide,4));  // 分割卡号
echo $split_card.'<br/>';
echo Mtool::hidenStr($bankCard,-12,"*").'<br/>'; // 隐藏后12位
echo Mtool::hidenStr($bankCard,-12,"0").'<br/>'; // 后12位设置为0
echo Mtool::hidenStr($bankCard,-12,-5,"*").'<br/>'; // 后12的后5位的中间隐藏
echo "<pre>";
print_r($res);//927.2
exit;