<?php
namespace Muyuxuan;
class Install
{

    /**
     * Install
     * @return void
     */
    public static function install()
    {
        static::installByRelation();
    }

    /**
     * Uninstall
     * @return void
     */
    public static function uninstall()
    {
        self::uninstallByRelation();
    }

    /**
     * installByRelation
     * @return void
     */
    public static function installByRelation()
    {
        $todir = dirname(dirname(dirname(dirname(__DIR__)))).'/config';
        $thisdir = __DIR__.'/config';
        if(is_dir($thisdir)){
            self::copy_dir($thisdir,$todir);
        }
        echo "完成config安装".PHP_EOL;
    }

    /**
     * uninstallByRelation
     * @return void
     */
    public static function uninstallByRelation()
    {
        foreach (static::$pathRelation as $source => $dest) {
            $path = base_path()."/$dest";
            if (!is_dir($path) && !is_file($path)) {
                continue;
            }
            echo "Remove $dest
";
            if (is_file($path) || is_link($path)) {
                unlink($path);
                continue;
            }
            remove_dir($path);
        }
    }

    /**
     * 复制到目录
     * @param string $source
     * @param string $dest
     * @param bool $overwrite
     * @return void\
     */
    public static function copy_dir(string $source, string $dest, bool $overwrite = false){
        if (is_dir($source)) {
            if (!is_dir($dest)) {
                mkdir($dest);
            }
            $files = scandir($source);
            foreach ($files as $file) {
                if ($file !== "." && $file !== "..") {
                    self::copy_dir("$source/$file", "$dest/$file");
                }
            }
        } else if (file_exists($source) && ($overwrite || !file_exists($dest))) {
            copy($source, $dest);
        }
    }
    
}