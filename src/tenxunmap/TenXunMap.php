<?php

namespace Muyuxuan\tenxunmap;

use Muyuxuan\http\Http;

/**
 * 腾讯地图开放平台
 */
class TenXunMap
{
    public static $key = 'HW5BZ-JCG3W-KWTRU-3GWDQ-PZ2YV-NKBYI';

    /**经纬度获取详情
     * @param int|string $long 经度
     * @param int|string $lat 纬度
     * @param string $key 秘钥
     * @return mixed
     */
    public static function longAndLatGetInfo(mixed $long,mixed $lat,?string $key=''){
        if(empty($key)){
            $key = self::$key;
        }
        $url = "https://apis.map.qq.com/ws/geocoder/v1/?location={$lat},{$long}&key={$key}";
        $res = Http::get($url);
        return json_decode($res,true);
    }

    /**
     * 地址获取详情
     * @param string $address 详细地址
     * @param string $key 秘钥
     * @return mixed
     */
    public static function addressGetInfo(string $address,?string $key=''){
        if(empty($key)){
            $key = self::$key;
        }
        $url = "https://apis.map.qq.com/ws/geocoder/v1/?address={$address}&key={$key}";
        $res = Http::get($url);
        return json_decode($res,true);
    }

    /**
     * ip获取详情
     * @param string $ip
     * @param string $key 秘钥
     * @return mixed
     */
    public static function ipgetInfo(string $ip,?string $key=''){
        if(empty($key)){
            $key = self::$key;
        }
        $url = "https://apis.map.qq.com/ws/location/v1/ip?ip={$ip}&key={$key}";
        $res = Http::get($url);
        return json_decode($res,true);
    }

    /**
     * 路线规划
     * @param string $fromLong 出发经度
     * @param string $fromLat 出发纬度
     * @param string $toLong 到达经度
     * @param string $toLat 到达纬度
     * @param array $event 其他参数
     * @param string $key 秘钥
     * @return mixed
     */
    public static function directionRouting(string $fromLong,string $fromLat,string $toLong,string $toLat,array $event=[],?string $key=''){
        $str = "";
        if(!empty($event)){
            $str = implode('&',$event);
        }
        if(!empty($str)){
            $str = "&".$str;
        }
        if(empty($key)){
            $key = self::$key;
        }
        $url = "https://apis.map.qq.com/ws/direction/v1/driving/?from={$fromLat},{$fromLong}&to={$toLat},{$toLong}&output=json&key={$key}{$str}";
        $res = Http::get($url);
        return json_decode($res,true);
    }
}