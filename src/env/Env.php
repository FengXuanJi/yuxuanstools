<?php
namespace Muyuxuan\env;


class Env
{
    static $initialize;

    /**
     * 获取环境变量值
     * @access public
     * @param  string $name    环境变量名（支持二级 . 号分割）
     * @param  string $default 默认值
     * @return mixed
     */
    public static function get($name, $default = null)
    {
        // 初始化一次
        if(!self::$initialize){
            self::init();
            self::$initialize = true;
        }
        $result = getenv(ENV_PREFIX . strtoupper(str_replace('.', '_', $name)));
        if (false !== $result) {
            if ('false' === $result) {
                $result = false;
            } elseif ('true' === $result) {
                $result = true;
            }

            return $result;
        }

        return $default;
    }

    /**
     * 初始化设置环境变量值（只需要执行一次，所以建议放入口文件执行）
     * @access public
     * @return mixed
     */
    private static function init()
    {
        defined('ENV_PREFIX') or define('ENV_PREFIX', 'PHP_'); // 环境变量的配置前缀
        $envPath = "";
        if(function_exists("config")){
            $envPath = config("plugin.muyuxuan.webmanenv.app.base_path");
        }
        if(empty($envPath)){
            if(function_exists("base_path")){
                $envPath = base_path();
            }
        }
        if(empty($envPath)){
            $envPath = dirname(dirname(dirname(dirname(dirname(__DIR__)))));
        }
        $envPath = $envPath.'/';

        $suffix = include_once __DIR__.'/webmanenv.php';
        foreach ($suffix as $suff){
            if (is_file($envPath . '.'.$suff)) {
                $env = parse_ini_file($envPath . '.env', true);
                foreach ($env as $key => $val) {
                    $name = ENV_PREFIX . strtoupper($key);
                    if (is_array($val)) {
                        foreach ($val as $k => $v) {
                            $item = $name . '_' . strtoupper($k);
                            putenv("$item=$v");
                        }
                    } else {
                        putenv("$name=$val");
                    }
                }
            }
        }

    }
}