<?php

namespace Muyuxuan\position;
use Muyuxuan\http\Http;

class Position
{
    protected static $config = [
//        'SecretID'=>'AKIDOPeJwv6wgOt7fTy2yF52tW598yA6j3ZfCtax',
//        'SecretKey'=>'30x7oR3rp06Ca85d64Z738m7bb2pQ233w9zlXXA5',
//        'tenXun_key'=>'HW5BZ-JCG3W-KWTRU-3GWDQ-PZ2YV-NKBYI',
        "maowenkehandkey"=>'a76702d5ae39c7e353e2cfcd2d21b3d5'
    ];
    protected static $message = '';

    /**经度纬度换详细城市
     * @param array|int[] $postion
     * @return array
     */
    public static function longAndLatGetCity(array $postion=['long'=>0,'lat'=>0]){
        $url = "https://hand.facai.maowenke.cn/api/postion/longandlattocity";
        $postion['key'] = self::$config['maowenkehandkey'];
        $res = Http::post($url,$postion);
        try {
            $res = json_decode($res,true);
        }catch (\Exception $e){
            return [false,$e->getMessage()];
        }
        if(isset($res['code'])){
            if($res['code']==1){
                return [true,$res['data']];
            }else{
                return [false,empty($res['msg'])?"未返回信息":$res['msg']];
            }
        }else{
            return [false,'请求失败'];
        }
    }

    /**
     * 城市详情换取经纬度
     * @param string $province 省份
     * @param string $city 城市
     * @param string $area 区
     * @param string $address 街道
     * @return array
     */
    public static function cityInfoToLongAndLat(string $province,string $city,string $area,string $address){
        $url = "https://hand.facai.maowenke.cn/api/postion/citytolongandlat";
        $array = [
            'province'=>$province,
            'city'=>$city,
            'area'=>$area,
            'address'=>$address,
            'key'=>self::$config['maowenkehandkey']
        ];
        $res = Http::post($url,$array);
        try {
            $res = json_decode($res,true);
        }catch (\Exception $e){
            return [false,$e->getMessage()];
        }
        if(isset($res['code'])){
            if($res['code']==1){
                return [true,$res['data']];
            }else{
                return [false,empty($res['msg'])?"未返回信息":$res['msg']];
            }
        }else{
            return [false,'请求失败'];
        }
    }

    /**ip换取城市详情
     * @param string $ip
     * @return array
     */
    public static function ipGetInfo(string $ip){
        $url = "https://hand.facai.maowenke.cn/api/postion/iptocityinfo";
        $array = [
            'ip'=>$ip,
            'key'=>self::$config['maowenkehandkey']
        ];
        $res = Http::post($url,$array);
        try {
            $res = json_decode($res,true);
        }catch (\Exception $e){
            return [false,$e->getMessage()];
        }
        if(isset($res['code'])){
            if($res['code']==1){
                return [true,$res['data']];
            }else{
                return [false,empty($res['msg'])?"未返回信息":$res['msg']];
            }
        }else{
            return [false,'请求失败'];
        }
    }

}