<?php

use Muyuxuanpay\redis\distance\MredisDistance;
use Muyuxuanpay\redis\lock\MredisLock;
use Muyuxuanpay\redis\ranking\MredisRanking;

require_once '../../vendor/autoload.php';
//锁
//获取锁，一直获取到锁为止
$res = MredisLock::gettLock('lock',true,10,500000);
print_r($res);//true
//获取锁，直接获取
$res = MredisLock::gettLock('lock',false,10,500000);
print_r($res);//true/false



//距离
$diliweizi = [
        [
            'name' => 'yabao_road',
            'long' => '116.43620200729366',
            'lat' => '39.916880160714435'
        ],
        [
            'name' => 'jianguomen',
            'long' => '116.4356870231628',
            'lat' => '39.908560377800676'
        ],
        [
            'name' => 'chaoyangmen',
            'long' => '116.4345336732864',
            'lat' => '39.924466658329585'
        ],
        [
            'name' => 'galaxy_soho',
            'long' => '116.4335788068771',
            'lat' => '39.921372916981106'
        ],
        [
            'name' => 'cofco',
            'long' => '116.43564410781856',
            'lat' => '39.92024564137184'
        ],
        [
            'name' => 'fesco',
            'long' => '116.435182767868',
            'lat' => '39.91811857809279'
        ],
    ];
$diliweizi1 = [
    'name' => 'fescoss',
    'long' => '116.435183767869',
    'lat' => '39.91811867809280'
];
//添加多元素
$res = MredisDistance::geoAdds('name',$diliweizi);
print_r($res);//true
//添加单元素
$res = MredisDistance::geoAdd('name',$diliweizi1);
print_r($res);//true
$address1 = 'fesco';
$address2 = 'galaxy_soho';
//获取标识之间的距离
$res = MredisDistance::geoDist($address1,$address2,'km');
print_r($res);//23.5
$address1 = [
    'long' => '116.435184467869',
    'lat' => '39.91811947809280'
];
$res = MredisDistance::geoRadius($address1,1000,'km',2000,"ASC");
print_r($res);//array

$res = MredisDistance::geoRadiusByMember($address2,1000,'m',1000,"ASC");
print_r($res);//array




//排行榜
$paihang = [
    [
        "name"=>"你好1",
        "score"=>10
    ],
    [
        "name"=>"你好2",
        "score"=>103
    ],
    [
        "name"=>"你好3",
        "score"=>77
    ],
    [
        "name"=>"你好4",
        "score"=>65
    ],
    [
        "name"=>"你好5",
        "score"=>89
    ],
    [
        "name"=>"你好6",
        "score"=>93
    ],
    [
        "name"=>"你好7",
        "score"=>127
    ],
    [
        "name"=>"你好8",
        "score"=>203
    ],
    [
        "name"=>"你好9",
        "score"=>109
    ],
    [
        "name"=>"你好10",
        "score"=>88
    ],
];
$paihang1 = [
    "name"=>"你好11",
    "score"=>999
];
$res = MredisRanking::Adds('name','score',$paihang);
print_r($res);//true

$res = MredisRanking::Add('name','score',$paihang1);
print_r($res);//true
$res = MredisRanking::getRanking(100,"DESC",true);
print_r($res);//array 有分数的数组
$res = MredisRanking::getMemberRanking('你好11',"DESC");
print_r($res);//int 排行
$res = MredisRanking::getMemberRange("你好11",5,"UP","DESC",true);
print_r($res);//array 向上排名取5个
$res = MredisRanking::getMemberRange("你好11",5,"DOW","DESC",true);
print_r($res);//array 向下排名取5个


