<?php


namespace Muyuxuan\redis\distance;


use Muyuxuan\redis\Other;

/**
 * redis求距离
 */
class MredisDistance
{
    use Other;
    protected static $defaultKey = 'mgeo';
    /**
     * 设置二维数组元素
     * @param string $field 作为标识的字段
     * @param array $data 位置信息(二维数组，每个元素必须包含3个元素$field字段,long=经度,lat=纬度)
     * @param string $key 保存的键名
     * @return bool
     */
    public static function geoAdds(string $field,array $data=[],string $key="mgeo"){
        self::setRedis();
        foreach ($data as $v){
            if(!empty($v[$field])){
                self::geoAdd($field,$v,$key);
            }
        }
        return true;
    }

    /**
     * 设置一维数组元素
     * @param string $field 作为标识的字段
     * @param array $data 位置信息(一维数组，必须包含3个元素$field字段,long=经度,lat=纬度)
     * @param string $key 保存的键名
     * @return bool
     */
    public static function geoAdd(string $field,array $data=[],string $key='mgeo'){
        self::setRedis();
        if(empty($data[$field])){
            return false;
        }
        self::$redis->geoadd($key,$data['long'],$data['lat'],$data[$field]);
        return true;
    }

    /**
     * 求两标识的距离
     * @param string $address1 标识1
     * @param string $address2 标识2
     * @param string $unit 距离单位 m=米, km=千米,mi=英里,ft=英尺
     * @param string $key 保存的键名
     * @return float
     */
    public static function geoDist(string $address1,string $address2,string $unit='m',string $key='mgeo'){
        self::setRedis();
        return self::$redis->geodist($key,$address1,$address2,$unit);
    }

    /**返回经纬度最大距离的数据
     * @param array $data 位置信息(一维数组，必须包含2个元素,long=经度,lat=纬度)
     * @param float $radius 半径
     * @param string $unit 距离单位 m=米, km=千米,mi=英里,ft=英尺
     * @param int $count 返回条数默认100
     * @param string $sort 排序模式ASC/DESC 顺序/倒序
     * @param string $key 保存的键名
     * @return mixed
     */
    public static function geoRadius(array $data,float $radius,string $unit,int $count=100,string $sort="ASC",string $key="mgeo"){
        self::setRedis();
        return self::$redis->georadius($key,$data['long'],$data['lat'],$radius,$unit,["WITHCOORD",'WITHDIST',$sort,'COUNT'=>$count]);
    }

    /**求标识经纬度最大距离的数据
     * @param string $address 标识1
     * @param float $radius 半径
     * @param string $unit 距离单位 m=米, km=千米,mi=英里,ft=英尺
     * @param int $count 返回条数默认100
     * @param string $sort 排序模式ASC/DESC 顺序/倒序
     * @param string $key 保存的键名
     */
    public static function geoRadiusByMember(string $address,float $radius,string $unit='m',int $count=100,string $sort="ASC",string $key="mgeo"){
        self::setRedis();
        return self::$redis->georadiusbymember($key,$address,$radius,$unit,["WITHCOORD",'WITHDIST',$sort,'COUNT'=>$count]);
    }

    /**
     * 删除某个键名
     * @param string $key 保存的键名
     * @return int
     */
    public static function geoDel(string $key='mgeo'){
        self::setRedis();
        return self::$redis->del($key);
    }

}